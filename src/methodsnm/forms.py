from abc import ABC, abstractmethod
import numpy as np
from numpy import array
from methodsnm.trafo import *
from methodsnm.mesh import *
from scipy import sparse
from methodsnm.fes import *

class Form(ABC):
    '''Abstract class used for the assembly (via connectivity matrices) of linear or bilinear forms.'''
    integrals = None
    fes = None
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def add_integrator(self, integrator):
        self.integrals.append(integrator)

    def __iadd__(self, integrator):
        self.add_integrator(integrator)
        return self

class LinearForm(Form):
    ''' This class represents linear forms used in FEM.'''

    vector = None
    def __init__(self, fes=None):
        self.fes = fes
        self.integrals = []
        self.bnd_integrals = []
    
    def assemble(self):
        '''Assembly of linear forms (in the variational form of the PDE) used for FEM.'''
        self.vector = np.zeros(self.fes.ndof)
        mesh = self.fes.mesh
        for bnd, ints in [[False,self.integrals],[True,self.bnd_integrals]]:
            for elnr, verts in enumerate(mesh.elements(bndry=bnd)):
                trafo = mesh.trafo(elnr,bndry=bnd)
                fe = self.fes.finite_element(elnr,bndry=bnd)
                dofs = self.fes.element_dofs(elnr,bndry=bnd)
                for integral in ints:
                    self.vector[dofs] += integral.compute_element_vector(fe, trafo)

    def __iadd__(self, integrator):
        if integrator.boundary:
            self.bnd_integrals.append(integrator)
        else:
            self.add_integrator(integrator)
        return self

class BilinearForm(Form):
    ''' This class represents linear forms used in FEM.'''

    matrix = None
    def __init__(self, fes=None, fes_test=None, fes_trial=None):
        if fes is None and (fes_test is None or fes_trial is None) or all([f is not None for f in [fes,fes_test,fes_trial]]):
            raise Exception("Invalid arguments, specify either `fes` or `fes_test` and `fes_trial`")
        if fes_test is None:
            fes_test = fes
            fes_trial = fes

        self.fes_trial = fes_trial
        self.fes_test = fes_test
        self.fes = fes_test
        self.integrals = []
        self.bnd_integrals = []
    
    def assemble(self):
        '''Assembly of bilinear forms (in the variational form of the PDE) used for FEM.'''
        self.matrix = sparse.lil_matrix((self.fes_test.ndof, self.fes_trial.ndof))
        mesh = self.fes.mesh
        for bnd, ints in [[False,self.integrals],[True,self.bnd_integrals]]:
            for elnr, verts in enumerate(mesh.elements(bndry=bnd)):
                trafo = mesh.trafo(elnr,bndry=bnd)
                fe_test = self.fes_test.finite_element(elnr,bndry=bnd)
                dofs_test = self.fes_test.element_dofs(elnr,bndry=bnd)
                fe_trial = self.fes_trial.finite_element(elnr,bndry=bnd)
                dofs_trial = self.fes_trial.element_dofs(elnr,bndry=bnd)
                elmat = np.zeros((len(dofs_test) , len(dofs_trial)))
                for integral in ints:
                    ret= integral.compute_element_matrix(fe_test, fe_trial, trafo)
                    #print(ret)
                    elmat += ret
                for i, dofi in enumerate(dofs_test):
                    for j, dofj in enumerate(dofs_trial):  
                        self.matrix[dofi,dofj] += elmat[i,j]
        self.matrix = self.matrix.tocsr()

    def __iadd__(self, integrator):
        if integrator.boundary:
            self.bnd_integrals.append(integrator)
        else:
            self.add_integrator(integrator)
        return self


from methodsnm.intrule import select_integration_rule
from numpy.linalg import det
def compute_difference_L2(uh, uex, mesh, intorder=5):
    ''' Computes the error of the approximate solution in the L2 norm.
    
    Inputs:
    uh (methodsnm.FEFunction): Approximate solution of the PDE.
    uex (methodsnm.GlobalFuction): Exact solution of the PDE.
    mesh(methodsnm.mesh): Mesh used for FEM/PUFEM.
    intorder(int): Integration order to select a sufficiently accurate integration rule.

    Returns:
    float:  L2 norm of u_approx-e_exact
    '''
    sumint = 0
    for elnr in range(len(mesh.elements())):
        trafo = mesh.trafo(elnr)
        intrule = select_integration_rule(intorder, trafo.eltype)
        uhvals = uh.evaluate(intrule.nodes, trafo)
        uexvals = uex.evaluate(intrule.nodes, trafo)
        diff = np.zeros(len(intrule.nodes))
        diff = (uhvals - uexvals)**2
        F = trafo.jacobian(intrule.nodes)
        w = array([abs(det(F[i,:,:])) * intrule.weights[i] for i in range(F.shape[0])])
        sumint += np.dot(w, diff)
    return np.sqrt(sumint)




