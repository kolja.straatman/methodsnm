from abc import ABC, abstractmethod
import numpy as np
from numpy import array, einsum
from methodsnm.intrule import *
from methodsnm.fe import *
from numpy.linalg import det, inv
from methodsnm.meshfct import ConstantFunction
from methodsnm.mesh import Mesh

class FormIntegral(ABC):
    '''Abstract base class for integration of different forms.'''
    boundary = False
    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

class LinearFormIntegral(FormIntegral):
    '''Abstract base class for integration of linear forms.'''

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def compute_element_vector(self, fe, trafo):
        raise NotImplementedError("Not implemented")

class SourceIntegral(LinearFormIntegral):

    ''' This class represents integrals the form \int f *v dx. (f known, v shape funtion)'''

    def __init__(self, coeff=ConstantFunction(1),intorder=None):
        self.coeff = coeff
        self.intorder = intorder

    def compute_element_vector(self, fe, trafo, intrule = None):
        '''Computes values of integral at given integration points.
        
        Inputs: 
        fe (methodsnm.fe): finite element that is used.
        trafo(function): Maps points from the reference element to local coordinates in the mesh. (returns numpy.ndarray)
        intrule: Integration rule used to evaluate the integral numerically.

        Returns:
        numpy.ndarray    Evaluation of the integral at given integration points.
        shape: (1,number of integration points in one finite element)
        '''
        if intrule is None:
            if self.intorder != None and self.intorder > 0:
                intrule = select_integration_rule(self.intorder, fe.eltype)
            else:
                intrule = select_integration_rule(2*fe.order, fe.eltype)
            
        shapes = fe.evaluate(intrule.nodes,trafo)
        coeffs = self.coeff.evaluate(intrule.nodes,trafo)
        weights = [w*c*abs(det(trafo.jacobian(ip))) for ip,w,c in zip(intrule.nodes,intrule.weights,coeffs)]
        ret= np.dot(shapes.T, weights)
        return ret


class NeumannBoundary(LinearFormIntegral):
    ''' This class represents boundary integrals the form \int g*v ds. (g known, v shape funtion)'''

    def __init__(self,coeff=ConstantFunction(1),intorder=None):
        self.coeff = coeff
        self.boundary=True
        self.intorder = intorder
        

    def compute_element_vector(self, fe, trafo, intrule = None):
        '''Computes values of integral at given integration points.
        
        Inputs: 
        fe (methodsnm.fe): finite element that is used.
        trafo(function): Maps points from the reference element to local coordinates in the mesh. (returns numpy.ndarray)
        intrule: Integration rule used to evaluate the integral numerically.

        Returns:
        numpy.ndarray    Evaluation of the integral at given integration points on the boundary.
        shape: (1,number of integration points in one finite element)
        '''
        if intrule is None:
            if self.intorder != None and self.intorder > 0:
                intrule = select_integration_rule(self.intorder, fe.eltype)
            else:
                intrule = select_integration_rule(2*fe.order, fe.eltype)
        shapes = fe.evaluate(intrule.nodes,trafo)
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        weights = [w*c*np.linalg.norm(trafo.jacobian(ip)) for ip,w,c in zip(intrule.nodes,intrule.weights,coeffs)]
        ret= np.dot(shapes.T, weights)
        return ret



class BilinearFormIntegral(FormIntegral):
    '''Abstract base class for integration of bilinear forms.'''
    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def compute_element_matrix(self, fe, trafo):
        raise NotImplementedError("Not implemented")

class BoundaryMassIntegral(BilinearFormIntegral):
    ''' This class represents boundary integrals the form \int u*v ds. (u,v shape funtions)'''

    def __init__(self, coeff=ConstantFunction(1),intorder=None):
        self.coeff = coeff
        self.boundary=True
        self.intorder = intorder

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        '''Computes values of integral at given integration points.
        
        Inputs: 
        fe_test (methodsnm.fe): finite element that is used for the test functions.
        fe_trial (methodsnm.fe): finite element that is used for the trial functions.
        trafo(function): Maps points from the reference element to local coordinates in the mesh. (returns numpy.ndarray)
        intrule: Integration rule used to evaluate the integral numerically.

        Returns:
        numpy.ndarray    Evaluation of the integral at given integration points on the boundary.
        shape: (number of integration points in one boundary finite element ,number of integration points in one boundary finite element)
        '''
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            if self.intorder != None and self.intorder > 0:
                intrule = select_integration_rule(self.intorder, fe_test.eltype)
            else:
                intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        shapes_test = fe_test.evaluate(intrule.nodes,trafo)
        shapes_trial = fe_test.evaluate(intrule.nodes,trafo)     
        coeffs = self.coeff.evaluate(intrule.nodes,trafo)
        F = trafo.jacobian(intrule.nodes)
        adetF = array([np.linalg.norm(F[i,:,:]) for i in range(F.shape[0])])
        ret = einsum("ij,ik,i,i,i->jk", shapes_test, shapes_trial, adetF, coeffs, intrule.weights)
        return ret


class MassIntegral(BilinearFormIntegral):
    ''' This class represents integrals the form \int u*v dx. (u,v shape funtions)'''

    def __init__(self, coeff=ConstantFunction(1),intorder=None):
        self.coeff = coeff
        self.intorder = intorder

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        '''Computes values of integral at given integration points.
        
        Inputs: 
        fe_test (methodsnm.fe): finite element that is used for the test functions.
        fe_trial (methodsnm.fe): finite element that is used for the trial functions.
        trafo(function): Maps points from the reference element to local coordinates in the mesh. (returns numpy.ndarray)
        intrule: Integration rule used to evaluate the integral numerically.

        Returns:
        numpy.ndarray    Evaluation of the integral at given integration points on the boundary.
        shape: (number of integration points in one finite element ,number of integration points in one finite element)
        '''
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            if self.intorder != None and self.intorder > 0:
                intrule = select_integration_rule(self.intorder, fe_test.eltype)
            else:
                intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        shapes_test = fe_test.evaluate(intrule.nodes,trafo)
        shapes_trial = fe_test.evaluate(intrule.nodes,trafo)     
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        F = trafo.jacobian(intrule.nodes)
        adetF = array([abs(det(F[i,:,:])) for i in range(F.shape[0])])
        ret = einsum("ij,ik,i,i,i->jk", shapes_test, shapes_trial, adetF, coeffs, intrule.weights)
        return ret
    

    
class LaplaceIntegral(BilinearFormIntegral):
    ''' This class represents integrals the form \int \grad u * \grad v dx. (u,v shape funtions)'''

    def __init__(self, coeff=ConstantFunction(1),intorder=None):
        self.coeff = coeff
        self.intorder = intorder

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        '''Computes values of integral at given integration points.
        
        Inputs: 
        fe_test (methodsnm.fe): finite element that is used for the test functions.
        fe_trial (methodsnm.fe): finite element that is used for the trial functions.
        trafo(function): Maps points from the reference element to local coordinates in the mesh. (returns numpy.ndarray)
        intrule: Integration rule used to evaluate the integral numerically.

        Returns:
        numpy.ndarray    Evaluation of the integral at given integration points on the boundary.
        shape: (number of integration points in one finite element ,number of integration points in one finite element)
        '''
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            if self.intorder != None and self.intorder > 0:
                intrule = select_integration_rule(self.intorder, fe_test.eltype)
            else:
                intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        dshapes_ref_test = fe_test.evaluate(intrule.nodes,trafo, deriv=True)
        dshapes_ref_trial = fe_test.evaluate(intrule.nodes,trafo, deriv=True)
        F = trafo.jacobian(intrule.nodes)
        adetF = array([abs(det(F[i,:,:])) for i in range(F.shape[0])])
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        if fe_test.PUFEM==False:
            invF = array([inv(F[i,:,:]) for i in range(F.shape[0])])
            ret = einsum("ijk,imn,ijl,iml,i,i,i->kn", dshapes_ref_test, dshapes_ref_trial, invF, invF, adetF, coeffs, intrule.weights)
        else:
            ret = einsum("ilk,iln,i,i,i->kn", dshapes_ref_test, dshapes_ref_trial, adetF, coeffs, intrule.weights)
        return ret


class PUFEM_LaplaceIntegral(BilinearFormIntegral):
    ''' Out datet class that represents boundary integrals the form \int \grad u * \grad v dx
      (u,v shape funtions). Works together with PUFEM_evaluate in methodsnm.fe.
      '''

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff
        

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        '''Computes values of integral at given integration points.
        
        Inputs: 
        fe_test (methodsnm.fe): finite element that is used for the test functions.
        fe_trial (methodsnm.fe): finite element that is used for the trial functions.
        trafo(function): Maps points from the reference element to local coordinates in the mesh. (returns numpy.ndarray)
        intrule: Integration rule used to evaluate the integral numerically.

        Returns:
        numpy.ndarray    Evaluation of the integral at given integration points on the boundary.
        shape: (number of integration points in one finite element ,number of integration points in one finite element)
        '''
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        dshapes_ref_test = fe_test.PUFEM_evaluate(intrule.nodes,trafo, deriv=True)
        dshapes_ref_trial = fe_test.PUFEM_evaluate(intrule.nodes,trafo, deriv=True)
        F = trafo.jacobian(intrule.nodes)
        adetF = array([abs(det(F[i,:,:])) for i in range(F.shape[0])])
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        ret = einsum("ijk,imn,i,i,i->kn", dshapes_ref_test, dshapes_ref_trial, adetF, coeffs, intrule.weights)
        return ret 
