import numpy as np

class approx_funct():
    '''This class contains local approximation basis needed 
    for the implementation of PUFEM (for the Laplace equation and the Helmholtz equation)'''

    def __init__(self):
        pass

    def harmonic_pol(order,ip,z=np.array([0,0]),deriv=False):
        '''Computes a basis of harmonic polynomials (Laplace equation) in 2D.
        
           Inputs:
           order(int): The approximation (interpolation) order.
           ip (numpy.array,shape:(2,1)): The integration point at which to evaluate the finite element.
           z (numpy.array,shape:(2,1)): Mesh nodes used for local approximation.
           deriv (bool): Whether to evaluate the derivative of the local approximation (or identity).

           Returns:
           numpy.array: The values of the local approximation basis at given integration points 
           around given nodes.
           shape:   (2*order+1)     (for deriv=False)
                    (2,2*order+1)   (for deriv=True)
        '''
        x,y=z

        if deriv==False:
            basis=np.empty((2*order+1))
            basis[0]=1
            for i in range(1,order+1):
                basis[2*i-1]=np.real(((ip[0]+ip[1]*1j)-(x+1j*y))**i)
                basis[2*i]=np.imag(((ip[0]+ip[1]*1j)-(x+1j*y))**i)
            return basis
        
        else:  
            basis=np.empty((2,2*order+1))
            basis[:,0]=0
            for i in range(1,order+1):
                    basis[0,2*i-1]=np.real(i*((ip[0]+ip[1]*1j)-(x+1j*y))**(i-1))
                    basis[0,2*i]=np.imag(i*((ip[0]+ip[1]*1j)-(x+1j*y))**(i-1))
                    basis[1,2*i-1]=np.real(i*1j*((ip[0]+ip[1]*1j)-(x+1j*y))**(i-1))
                    basis[1,2*i]=np.imag(1j*i*((ip[0]+ip[1]*1j)-(x+1j*y))**(i-1))
            return basis
           
     

    def plane_waves(order,ip,z=None,wavenumber=5,deriv=False):
        '''Computes plane waves basis to solve a homogeneous Helmholtz equation in 2D.
        
           Inputs:
           order(int): The approximation (interpolation) order.
           ip (numpy.array,shape:(2,1)): The integration point at which to evaluate the finite element.
           deriv (bool): Whether to evaluate the derivative of the local approximation (or identity).
           wavenumber (int): Value of the wave number (has to be larger than 0).

           Returns:
           numpy.array: The values of the local approximation basis at given integration points 
           around given nodes.
           shape:   (2*order+1)     (for deriv=False)
                    (2,2*order+1)   (for deriv=True)
        
        '''
        
        if deriv == False:
            basis = np.empty((2*order+1))
            for i in range(2*order+1):
                theta = 2*np.pi*i/(2*order+1)
                basis[i] = np.real(np.exp(1j*wavenumber*((ip[0])*np.cos(theta)+(ip[1])*np.sin(theta))))
            return basis 
    
        else:
            basis = np.empty((2,2*order+1))
            for i in range(2*order+1):
                theta = 2*np.pi*i/(2*order+1)
                basis[0,i] = np.real(1j*wavenumber*np.cos(theta)*np.exp(1j*wavenumber*((ip[0])*np.cos(theta)+(ip[1])*np.sin(theta))))
                basis[1,i] = np.real(1j*wavenumber*np.sin(theta)*np.exp(1j*wavenumber*((ip[0])*np.cos(theta)+(ip[1])*np.sin(theta))))
            return basis
    