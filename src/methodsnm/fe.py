from abc import ABC, abstractmethod
import numpy as np
from numpy import array
from methodsnm.approx_pol import approx_funct
from numpy.linalg import det, inv
from methodsnm.mesh_2d import StructuredRectangleMesh


class FE(ABC):
    """
    This is the base class for all **scalar** finite element classes. 
    It provides a template for the evaluate method.
    """
    ndof = None # number of degrees of freedom
    order = None # polynomial order
    eltype = None # e.g. "segment", "triangle", ...
    dim = None # dimension of the domain


   
    def __init__(self):
        self.PUFEM=False
        

    @abstractmethod
    def _evaluate_id(self, ip,trafo=None):
    
        """
        Evaluates the finite element at the given integration point.

        Parameters:
        ip (numpy.array): The integration point at which to evaluate the finite element.

        Returns:
        numpy.array: The values of the finite element basis fcts. at the given integration point.
        """
        raise Exception("Not implemented - Base class should not be used")
    
    @abstractmethod
    def _PUFEM_evaluate_id(self, ip,trafo):
        raise Exception("Not implemented - Base class should not be used")
    


        
    def _evaluate_id_array(self, ips,trafo=None):
        """
        Evaluates the finite element at multiple integration points at once.
        Base class implementation is a simple loop over the integration points.
        Performance gains can only be obtained by overwriting this method.

        Parameters:
        ips (numpy.array): The integration point at which to evaluate the finite element.

        Returns:
        numpy.ndarray: The values of the finite element at the given integration points.
                       shape: (len(ips), ndof)
        """
        ret = np.empty((len(ips), self.ndof ))
        for i in range(len(ips)):
            ret[i,:] = self._evaluate_id(ips[i])
        return ret

    def _evaluate_deriv_array(self, ips,trafo=None):
        """
        Evaluates the derivative of finite element at multiple integration points at once.
        Base class implementation is a simple loop over the integration points.
        Performance gains can only be obtained by overwriting this method.

        Parameters:
        ips (numpy.array): The integration point at which to evaluate the finite element.

        Returns:
        numpy.ndarray: The values of the finite element at the given integration points.
                       shape: (len(ips), dim, ndof)
        """
        ret = np.empty((len(ips), self.dim, self.ndof))
        for i in range(len(ips)):
            ret[i,:,:] = self._evaluate_deriv(ips[i])
        return ret
    
    def _PUFEM_evaluate_id_array(self, ips,trafo):
     
        ret = np.empty((len(ips), self.ndof ))
        for i in range(len(ips)):
            ret[i,:] = self._PUFEM_evaluate_id(ips[i],trafo)
        return ret

    def _PUFEM_evaluate_deriv_array(self, ips,trafo):
        
        ret = np.empty((len(ips), self.dim, self.ndof))
        for i in range(len(ips)):
            ret[i,:,:] = self._PUFEM_evaluate_deriv(ips[i],trafo)
        return ret

    @abstractmethod
    def _evaluate_deriv(self, ip,trafo=None):
        """
        Evaluates the derivative of a finite element at the given integration point.

        Parameters:
        ip (numpy.array): The integration point at which to evaluate the finite element.

        Returns:
        numpy.array: The values of the derivative of the finite element basis fcts. at the given integration point.
                     shape (dim, ndof)
        """
        raise Exception("Not implemented - Base class should not be used")
    
    @abstractmethod
    def _PUFEM_evaluate_deriv(self, ip,trafo):
        raise Exception("Not implemented - Base class should not be used")
    


    def evaluate(self, ip,trafo, deriv=False):
        """
        Evaluates the (derivative of) finite element or a partition of unity finite element at given integration point(s).

        Parameters:
        ip (numpy.array): The integration point(s) at which to evaluate the finite element.
        deriv (bool): Whether to evaluate the derivative of the finite element (or identity).
        trafo (function): Transformation function that maps points from the reference element to elements in the mesh. (returns a numpy.ndarray)

        Returns:
        For FEM:
        numpy.array: The values of the finite element basis fcts. at the given integration point.
           shape:                  (ndof) (for single ip) 
                  or          (dim, ndof) (for single ip and deriv = True)
                  or (len(ip),      ndof) (for multiple ips)
                  or (len(ip), dim, ndof) (for multiple ips and deriv = True)
        
        For PUFEM:
        numpy.array: The values of the finite element basis fcts. at the given integration point.
           shape:                  (ndof*len(harmonic basis)) (for single ip) 
                  or          (dim, ndof*len(harmonic basis)) (for single ip and deriv = True)
                  or (len(ip),      ndof*len(harmonic basis)) (for multiple ips)
                  or (len(ip), dim, ndof*len(harmonic basis)) (for multiple ips and deriv = True)
        """


        if self.PUFEM==True:
            if isinstance(ip, np.ndarray):
                if ip.ndim == 1:
                    if deriv:
                        return self._PUFEM_evaluate_deriv(ip,trafo)
                    else:
                        return self._PUFEM_evaluate_id(ip,trafo)
                else:
                    if deriv:
                        return self._PUFEM_evaluate_deriv_array(ip,trafo)
                    else:
                        return self._PUFEM_evaluate_id_array(ip,trafo)
                        
            else:
                raise Exception("Invalid input")
        else:
            if isinstance(ip, np.ndarray):
                if ip.ndim == 1:
                    if deriv:
                        return self._evaluate_deriv(ip,trafo=None)
                    else:
                        return self._evaluate_id(ip,trafo=None)
                else:
                    if deriv:
                        return self._evaluate_deriv_array(ip,trafo=None)
                    else:
                        return self._evaluate_id_array(ip,trafo=None)
                        
                    
            else:
                raise Exception("Invalid input")
        
    
    def PUFEM_evaluate(self, ip,trafo, deriv=False):
        '''Dated implementation that 
        evaluates the partition of unity finite elements at given integration point.
        Works together with PUFEM_LaplaceIntegral in formint.py. Was created for the initial implementation.

        Same inputs and return as evaluate for PUFEM.
          '''
        if isinstance(ip, np.ndarray):
            if ip.ndim == 1:
                if deriv:
                    return self._PUFEM_evaluate_deriv(ip,trafo)
                else:
                    return self._PUFEM_evaluate_id(ip,trafo)
            else:
                if deriv:
                    return self._PUFEM_evaluate_deriv_array(ip,trafo)
                else:
                    return self._PUFEM_evaluate_id_array(ip,trafo)
        else:
            raise Exception("Invalid input")
    
class Lagrange_FE(FE):
    """
    This class represents a Lagrange finite element.
    A Lagrange finite element associates the dofs with the nodes of the element.
    """
    nodes = None

    def __str__(self):
        return f"Lagrange-FE-obj(order={self.order},nodes={self.ndof})"



class Node_FE(FE):
    """
    FE for a point (node).
    """    

    def __init__(self):
        self.eltype = "point"
        self.dim = 0
        self.ndof = 1

    def _evaluate_id(self, ip,trafo=None):
        return np.ones((1,))

    def _evaluate_id_array(self, ip,trafo=None):
        return np.ones((len(ip),1))

    def _evaluate_deriv(self, ip):
        raise Exception("Derivative of node FE should not be called")
    
    def _PUFEM_evaluate_deriv(self, ip,trafo):
        pass

    def _PUFEM_evaluate_id(self, ip,trafo):
        pass

    
from methodsnm.fe_2d import P1_Triangle_FE

class PUFE(FE):
    '''This class represents a triangular partition of unity finite element in 2D.'''

    eltype = 'triangle' 
    FE.PUFEM=True

    def __init__(self,order=3,wavenumber=0):
        self.p1 = P1_Triangle_FE()
        self.order = order+1 #needs to be one higher for proper integration rule 
        self.approx_order = order
        self.nodes = self.p1.nodes
        self.wavenumber = wavenumber
        if self.wavenumber == 0:  
            self.node_ndof = 2*self.approx_order + 1
        elif self.wavenumber > 0:
            self.node_ndof = 2*self.approx_order + 1
        elif self.wavenumber < 0:
            raise ValueError('Wave number should be larger than zero.')
        self.ndof = self.node_ndof*len(self.nodes)
        self.dim = 2
        
        

    def _evaluate_id(self, ip,trafo=None):
        pass

    def _evaluate_deriv(self, ip,trafo=None):
        pass

    def _PUFEM_evaluate_id(self, ip,trafo):

        '''
        Evaluates the PUFEM basis function at given integration point in given local patch in 2D.

        Inputs:
        ip (numpy.ndarray): The integration point at which to evaluate the partition of unity basis.
        trafo (function): Maps points from the reference element to patches in the mesh. (returns a numpy.ndarray)

        Returns:
        numpy.array: The values of the finite element basis fcts. at the given integration point.
           shape:                  ndof*len(harmonic basis 
                  
        '''

        ret=np.empty((len(self.nodes),self.node_ndof))

        if trafo == None:
            mip = ip
            mnodes=np.array(self.nodes)
        else:
            mip=trafo(ip)
            nodes=np.array(self.nodes)
            mnodes=trafo(nodes)
            
        
        p1_ev=self.p1._evaluate_id(ip)

        for i in range (self.node_ndof):
            for k in range(len(self.nodes)):
                if self.wavenumber == 0:
                    ret[k][i]=p1_ev[k]*approx_funct.harmonic_pol(self.approx_order,mip,z=mnodes[k])[i]
                elif self.wavenumber > 0:
                    ret[k][i]=p1_ev[k]*approx_funct.plane_waves(self.approx_order,mip,wavenumber=self.wavenumber)[i]
        ret=np.reshape(ret,(self.ndof))
        return ret

        
    def _PUFEM_evaluate_deriv(self, ip,trafo):
        '''
        Evaluates the derivative of the PUFEM basis function at given integration point in given local patch in 2D.

        Inputs:
        ip (numpy.ndarray): The integration point at which to evaluate the partition of unity basis.
        trafo (function): Maps points from the reference element to patches in the mesh. (returns a numpy.ndarray)

        Returns:
        numpy.array: The values of the finite element basis fcts. at the given integration point.
           shape:                (dim, ndof*len(harmonic basis))
        '''
        
        if trafo == None:
            mip = ip
            mnodes=np.array(self.nodes)
        else:
            mip=trafo(ip)
            nodes=np.array(self.nodes)
            mnodes=trafo(nodes)
            
    
        F=trafo.jacobian(self.nodes)
        invF = array([inv(F[i,:,:]) for i in range(F.shape[0])])
        p1_ev=self.p1._evaluate_id(ip)
        ref_deriv=self.p1._evaluate_deriv(ip)
        p1_deriv=np.empty((len(self.nodes),self.dim))
        for l in range (len(self.nodes)):
            p1_deriv[l,:]=np.dot(invF[l,:,:].T,self.p1._evaluate_deriv(ip)[:,l])


        ret=np.empty((self.dim,len(self.nodes),self.node_ndof))
        for i in range (self.node_ndof):
            for k in range(self.dim):
                for j in range (len(self.nodes)):
                    if self.wavenumber == 0:
                        ret[k][j][i]=p1_deriv[j][k]*approx_funct.harmonic_pol(self.approx_order,mip,z=mnodes[j])[i]+p1_ev[j]*approx_funct.harmonic_pol(self.approx_order,mip,mnodes[j],deriv=True)[k][i]
                    elif self.wavenumber > 0:
                        ret[k][j][i]=p1_deriv[j][k]*approx_funct.plane_waves(self.approx_order,mip,wavenumber=self.wavenumber)[i]+p1_ev[j]*approx_funct.plane_waves(self.approx_order,mip,wavenumber=self.wavenumber,deriv=True)[k][i]
        ret=np.reshape(ret,(self.dim,self.ndof))
        return ret

    
from methodsnm.fe_1d import P1_Segment_FE
class PUFE_Segment(FE):
    ''' This class represents a segement partition of unity finite element in 1D. '''
    eltype = 'segment' 
    

    def __init__(self,order=3,wavenumber = 0):
        self.PUFEM=True
        self.p1= P1_Segment_FE()
        self.order=order+1
        self.approx_order=order
        self.nodes=self.p1.nodes
        self.wavenumber = wavenumber
        if self.wavenumber == 0 :  
            self.node_ndof = 2*self.approx_order + 1
        elif self.wavenumber > 0:
            self.node_ndof = 2*self.approx_order + 1
        elif self.wavenumber < 0:
            raise ValueError('Wave number should be larger than zero.')
        self.ndof=self.node_ndof*len(self.nodes)
        self.dim=1
        
                  
    def _evaluate_id(self, ip,trafo=None):
        pass

    def _evaluate_deriv(self, ip,trafo=None):
        pass

    def _PUFEM_evaluate_id(self, ip,trafo):

        '''
        Evaluates the PUFEM basis function at given integration point in given local patch in 1D.

        Inputs:
        ip (numpy.ndarray): The integration point at which to evaluate the partition of unity basis.
        trafo (function): Maps points from the reference element to patches in the mesh. (returns a numpy.ndarray)

        Returns:
        numpy.array: The values of the finite element basis fcts. at the given integration point.
           shape:                  ndof*len(harmonic basis 
                  
        '''

        ret=np.empty((len(self.nodes),self.node_ndof))

        if trafo == None:
            mip = ip
            mnodes=np.array(self.nodes)
        else:
            mip=trafo(ip)
            nodes=np.array(self.nodes)
            mnodes=trafo(nodes)
            
            

        p1_ev=self.p1._evaluate_id(ip)

        for i in range (self.node_ndof):
            for k in range(len(self.nodes)):
                if self.wavenumber == 0:
                    ret[k][i]=p1_ev[k]*approx_funct.harmonic_pol(self.approx_order,mip,z=mnodes[k])[i]
                elif self.wavenumber > 0:
                    ret[k][i]=p1_ev[k]*approx_funct.plane_waves(self.approx_order,mip,wavenumber=self.wavenumber)[i]
        ret=np.reshape(ret,(self.ndof))
        return ret

   
        
    def _PUFEM_evaluate_deriv(self, ip,trafo):

        '''
        Evaluates the derivative of the PUFEM basis function at given integration point in given local patch in 2D.

        Inputs:
        ip (numpy.ndarray): The integration point at which to evaluate the partition of unity basis.
        trafo (function): Maps points from the reference element to patches in the mesh. (returns a numpy.ndarray)

        Returns:
        numpy.array: The values of the finite element basis fcts. at the given integration point.
           shape:                (dim, ndof*len(harmonic basis))
        '''
        
        if trafo == None:
            mip = ip
            mnodes=np.array(self.nodes)
        else:
            mip=trafo(ip)
            nodes=np.array(self.nodes)
            mnodes=trafo(nodes)
            
    
        F=trafo.jacobian(self.nodes)
        invF=inv(F)
        p1_ev=self.p1._evaluate_id(ip)
        p1_deriv=np.empty((len(self.nodes),self.dim))
        for l in range (len(self.nodes)):
            p1_deriv[:,l]=np.dot(self.p1._evaluate_deriv(ip)[:,l],invF[l,:,:])

        ret=np.empty((self.dim,len(self.nodes),self.node_ndof))
        for i in range (self.node_ndof):
                for k in range(self.dim):
                    for j in range (len(self.nodes)):
                        if self.wavenumber == 0 :
                            ret[k][j][i]=p1_deriv[j][k]*approx_funct.harmonic_pol(self.approx_order,mip,z=mnodes[j])[i]+p1_ev[j]*approx_funct.harmonic_pol(self.approx_order,mip,z=mnodes[j],deriv=True)[k][i]
                        elif  self.wavenumber > 0:
                           ret[k][j][i]=p1_deriv[j][k]*approx_funct.plane_waves(self.approx_order,mip,wavenumber=self.wavenumber)[i]+p1_ev[j]*approx_funct.plane_waves(self.approx_order,mip,wavenumber=self.wavenumber,deriv=True)[k][i] 
        ret=np.reshape(ret,(2,self.ndof))
        return ret