    

def FEM_ndof(n,p):
    ''' Evaluates the number of degrees of freedom using FEM.
    
    Inputs:
    n (int): Number of patches (here: number of nodes).
    p (int): polynomial order

    Returns:
    (int)        Number of degrees of freedom
    '''
    edges=5
    for i in range (1, n):
        edges+=(2*i-1)*3+8
    faces=n*n*2
    vert=(n+1)*(n+1)

    return vert+(p-1)*edges+((p-1)*(p-2)//2)*faces

def PUFEM_harmonic_ndof(n,p):
    ''' Evaluates the number of degrees of freedom using PUFEM with harmonic polynomials or plane waves.

    Inputs:
    n (int): Number of patches (here: number of nodes).
    p (int): polynomial order

    Returns:
    (int)        Number of degrees of freedom
    '''
    vert=(n+1)*(n+1)
    M=2*p+1
    return vert * M

def PUFEM_standard_poly_ndof(n,p):
    ''' Evaluates the number of degrees of freedom using PUFEM with standard polynomials.
    
    Inputs:
    n (int): Number of patches (here: number of nodes).
    p (int): polynomial order

    Returns:
    (int)        Number of degrees of freedom
    '''
    vert = (n+1)*(n+1)
    M=(p+1)*(p+2)//2
    return vert * M


