from abc import ABC, abstractmethod
import numpy as np
from numpy import array

class IntRule:
    ''' Abstract base class for numerical integration rules.'''
    nodes = None
    weights = None
    exactness_degree = None
    def __init__(self):
        pass

    def integrate(self, f):
        '''Evaluates integral as a sum of weighted function values'''
        # f is assumed to be a function that takes a numpy array as input
        fvals = array([f(self.nodes[i]) for i in range(len(self.weights))])
        return np.dot(fvals.T, self.weights)

    def __str__(self):
        return f"Integration rule \"{self.__class__.__name__}\" with {len(self.nodes)} nodes (exactness degree {self.exactness_degree}):\nnodes = {self.nodes}\nweights = {self.weights}"

from methodsnm.intrule_1d import MidPointRule, NewtonCotesRule, NP_GaussLegendreRule
from methodsnm.intrule_2d import EdgeMidPointRule, DuffyBasedRule

npgauss_warned = False
def select_integration_rule(order, eltype):
    '''
    Selects a suitable integration rule for given exactness order and type of finite element.

    Inputs:
    order (int): Polynomial order of the integration rule.
    eltype (float: "segment" or "triangle"): Type of finite element we integrate on.

    Returns: Suiatble integration rule e.g Midpoint rule for integrals over a segment with polynomial order 1.
    '''
    global npgauss_warned
    if eltype == "segment":
        if order == 1:
            return MidPointRule()
        else:
            if npgauss_warned == False:
                #print("Warning: Using NumPy Gauss rules!")
                npgauss_warned = True
            return NP_GaussLegendreRule (n=order//2+1)
            #return NewtonCotesRule(n=order+1)
    elif eltype == "triangle":
        if order <= 1:
            return EdgeMidPointRule()
        else:
            return DuffyBasedRule(order)
    else:
        raise NotImplementedError("select_integration_rule only implemented for segments and triangles (not for " + eltype + ", yet)")

