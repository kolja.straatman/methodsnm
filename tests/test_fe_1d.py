import pytest
import import_hack
from methodsnm.fe_1d import *
from numpy.linalg import norm

try:
    from methodsnm.solution import *
except ImportError:
    pass

@pytest.mark.parametrize("sample", [[0.25, [0.75,0.25]],
                                    [0.75, [0.25,0.75]]])
def test_P1(sample):
    xval, yvals = sample
    p1 = P1_Segment_FE()
    x = np.array([xval])
    yvals = np.array(yvals)
    yvals -= p1.evaluate(x,trafo=None)
    assert norm(yvals) < 1e-14

