import unittest
import pytest
import import_hack
import numpy as np
from methodsnm.fe import *
from methodsnm.approx_pol import *
from methodsnm.mesh_2d import *
from methodsnm.fes import *
from numpy.linalg import eig
from methodsnm.forms import *
from methodsnm.formint import *
from methodsnm.meshfct import *

class Test_PUFEM_Basis(unittest.TestCase):

    def test_harmonic_basis(self):
        sol1 = np.array([1.,-2.,-2.])
        sol2 = np.array([1.,4.,-6.])
        sol3 = np.array([1.,2.,2.])
        sol4 = np.array([[0.,1.,0.,-4.,-4.],[0.,0.,1.,4.,-4.]])
        self.assertTrue((approx_funct.harmonic_pol(1,(1,2),(3,4))==sol1).all())
        self.assertTrue((approx_funct.harmonic_pol(1,(1,-2),(-3,4))==sol2).all())
        self.assertTrue((approx_funct.harmonic_pol(1,(-1,-2),(-3,-4))==sol3).all())
        self.assertTrue((approx_funct.harmonic_pol(2,(1,2),(3,4),deriv=True)==sol4).all())

    def test_planewave_basis(self):
        sol1 = np.array([1.,1.,1.])
        sol2 = np.array([0.54030231, 0.93375725, 0.20334287])
        sol3 = np.array([[-0.84147098,  0.17895348, -0.4895538],[ 0.,-0.30995652, -0.84793205]])
        self.assertTrue((approx_funct.plane_waves(1,(0,0))==sol1).all())
        np.testing.assert_almost_equal(approx_funct.plane_waves(1,(1,1),wavenumber=1),sol2)
        np.testing.assert_almost_equal(approx_funct.plane_waves(1,(1,1),wavenumber=1,deriv= True),sol3)

    
    def test_PUFEM_basis_harmonic_pol(self):
        pufe = PUFE(order=1)
        trafo = None
        sol1 = np.array([ 1.,0.,0.,0.,0., 0., 0., 0.,0.])
        sol2 = np.array([ 0.,0.,0.,1.,0., 0., 0., 0.,0.])
        sol3 = np.array([0.5,0.,0.25,0.,-0.,0.,0.5,0.,-0.25])
        sol4 = np.array ([0.,0.,0.,0.5,-0.25,0.25,0.5,0.25,-0.25])
        self.assertTrue((pufe.evaluate(np.array([0,0]),trafo)==sol1).all())
        self.assertTrue((pufe.evaluate(np.array([1,0]),trafo)==sol2).all())
        self.assertTrue((pufe.evaluate(np.array([0,0.5]),trafo)==sol3).all())
        self.assertTrue((pufe.evaluate(np.array([0.5,0.5]),trafo)==sol4).all())


    def test_PUFEM_basis_plane_waves(self):
        pufe1 = PUFE(order=1,wavenumber=5)
        pufe2 = PUFE(order=1,wavenumber=10)
        trafo = None
        sol1 = np.array([ 1.,1.,1.,0.,0., 0., 0., 0.,0.])
        sol2 = np.array([0.,0.,0.,0.,0.,0.,1.,-0.7217119766623314,-0.7217119766623314])
        sol3 = np.array([0.,0.,0.,0.1418310927316127,-0.12821684096870356,  0.42705953045008915
                        ,0.1418310927316127, -0.12821684096870356,  0.42705953045008915])
        self.assertTrue((pufe1.evaluate(np.array([0,0]),trafo)==sol1).all())
        self.assertTrue((pufe2.evaluate(np.array([0,0]),trafo)==sol1).all())
        np.testing.assert_almost_equal(pufe2.evaluate(np.array([0,1]),trafo),sol2)
        np.testing.assert_almost_equal(pufe2.evaluate(np.array([0.5,0.5]),trafo),sol3)



    #Tests how many eigenvalues of the stiffness matrix are zero (dimension of the kernel).
    #Here we use:
    #mesh size = 1 and first order PUFEM space with harmonic polynomials. 
    #    Therefore is the stiffness matrix a 12x12 csr_matrix.
    #We do the assembly to solve for given equation in PUFEM.ipynb (only bilinear form).
    #
    #The eigenvalues are {0,0,0,(\sqrt(586)+31)/30,(-\sqrt(586)+31)/30,\approx -0.279, \approx 0.151
    #                        ,\approx 0.309, \approx 0.545, \approx 1.488,\approx 2.754}.
    
    #So the dimension of the kernel is 3.

    def test_kernel_harmonic_pol(self):
        mesh = StructuredRectangleMesh(1,1)
        fes = PUFE_Space(mesh,order=1) 
        blf = BilinearForm(fes)
        c = GlobalFunction(lambda x: 1, mesh = mesh)
        blf+=LaplaceIntegral(c)
        blf+=BoundaryMassIntegral(c) 
        blf.assemble()

        mat = blf.matrix.toarray()
        vals,vect = eig(mat)
        dim_ker = 0
        for i in vals:
            if i<1e-14 and i>-1e-14:
                dim_ker += 1
        
        self.assertEqual(3,dim_ker)

    #Tests how many eigenvalues of the stiffness matrix are zero (dimension of the kernel).
    #Here we use:
    #mesh size = 1 and first order PUFEM space with harmonic polynomials. 
    #    Therefore is the stiffness matrix a 12x12 csr_matrix.
    #We do the assembly to solve for given equation in PUFEM.ipynb (only bilinear form).
    
    #The eigenvalues are approximately {-1.042,-0.262,-0.004,0.251,0.75,0.997,1.085,1.441,1.518,1.865,2.106,2.485}.
    
    #So the dimension of the kernel is 0.

    def test_kernel_plane_waves(self):
        mesh = StructuredRectangleMesh(1,1)
        fes = PUFE_Space(mesh,order=1,wavenumber=5) 
        blf = BilinearForm(fes)
        c = GlobalFunction(lambda x: 1, mesh = mesh)
        r = GlobalFunction(lambda x: -25, mesh = mesh)
        blf+=LaplaceIntegral(c)
        blf+=MassIntegral(r)
        blf+=BoundaryMassIntegral(c) 
        blf.assemble()

        mat = blf.matrix.toarray()
        vals,vect = eig(mat)
        dim_ker = 0
        for i in vals:
            if i<1e-14 and i>-1e-14:
                dim_ker += 1
        
        self.assertEqual(0,dim_ker)