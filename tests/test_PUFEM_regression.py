import unittest
from import_hack import *
from numpy import exp,cos,sin
from methodsnm.fe import PUFE
from methodsnm.mesh_2d import *
from methodsnm.fes import *
from methodsnm.forms import *
from methodsnm.formint import *
from methodsnm.meshfct import *
from scipy.sparse.linalg import spsolve

class Test_PUFEM_Regression(unittest.TestCase):

    def test_Laplace_Solution(self):

        error = []
        convergence = False
        for N in range (1,5):
            mesh = StructuredRectangleMesh(N,N)
            order = 1
            fes = PUFE_Space(mesh,order=order) 
            blf = BilinearForm(fes)

            def coeff_g(x):
                if x[0]<=1e-10:
                    return 0
                elif x[0]>=1-1e-10:
                    return 2*exp(1)*cos(x[1])
                elif x[1]<=1e-10:
                    return exp(x[0])
                elif x[1]>=1-1e-10:
                    return exp(x[0])*(cos(1)-sin(1))
                else:
                    raise Exception('')

            c = GlobalFunction(lambda x: 1, mesh = mesh)
            r = GlobalFunction(lambda x: 1, mesh = mesh)
            blf+=LaplaceIntegral(c)
            blf+=BoundaryMassIntegral(r) #r=alpha
            blf.assemble()

            lf = LinearForm(fes)
            f = GlobalFunction(lambda x: 0, mesh = mesh)
            lf += SourceIntegral(f)
            g=GlobalFunction(coeff_g,mesh=mesh)
            lf+= NeumannBoundary(g) #g=alpha*g_R
            lf.assemble()

            for i in range (fes.ndof):
                blf.matrix[i,i]+=1e-11

            uh = FEFunction(fes)
            uh.vector = spsolve(blf.matrix, lf.vector)
            uex =  GlobalFunction(lambda x: exp(x[0])*cos(x[1]), mesh = mesh)
            l2diff = compute_difference_L2(uh, uex, mesh, intorder = 6)
            error.append(l2diff)
        self.assertAlmostEqual(error[0],0,places=1)
        if error[0]>error[1]>error[2]>error[3]>0:
            convergence =True
        self.assertTrue(convergence)
        

    def test_Helmholtz_Solution(self):

        error = []
        convergence = False
        for N in range (2,6):
            mesh = StructuredRectangleMesh(N,N)
            order = 1
            fes = PUFE_Space(mesh,order=order,wavenumber=5) 
            
            def coeff_g(x):
                if x[0]<=1e-10:
                    return (sin(4*x[1])+cos(4*x[1]))*(-2)
                elif x[0]>=1-1e-10:
                    return (sin(4*x[1])+cos(4*x[1]))*(-2*sin(3)+4*cos(3))
                elif x[1]<=1e-10:
                    return (sin(3*x[0])+cos(3*x[0]))*(-3)
                elif x[1]>=1-1e-10:
                    return (sin(3*x[0])+cos(3*x[0]))*(-3*sin(4)+5*cos(4))
                else:
                    raise Exception('')

            blf = BilinearForm(fes)
            c = GlobalFunction(lambda x: 1, mesh = mesh)
            r = GlobalFunction(lambda x: -25, mesh = mesh)
            blf+=LaplaceIntegral(c)
            blf+= MassIntegral(r)
            blf+=BoundaryMassIntegral(c) #r=alpha
            blf.assemble()
            lf = LinearForm(fes)
            f = GlobalFunction(lambda x: 0, mesh = mesh)
            lf += SourceIntegral(f)
            g=GlobalFunction(coeff_g,mesh=mesh)
            lf+= NeumannBoundary(g) #g=alpha*g_R
            lf.assemble()

            uh = FEFunction(fes)
            uh.vector = spsolve(blf.matrix, lf.vector)
            uex =  GlobalFunction(lambda x: (cos(3*x[0])+sin(3*x[0]))*(sin(4*x[1])+cos(4*x[1])), mesh = mesh)
            l2diff = compute_difference_L2(uh, uex, mesh, intorder = 6)
            error.append(l2diff)
        self.assertAlmostEqual(error[3],0,places=1)
        if error[0]>error[1]>error[2]>error[3]>0:
            convergence =True
        self.assertTrue(convergence)